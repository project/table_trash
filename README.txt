TABLE TRASH QUICK INSTALL

For the bulk of its features, Table Trash relies on the DataTables jQuery
library. There are 3 ways to load the library. The version included with the
module, the datatables cdn, or an alternate cdn you specify. You can specify
which one you want to use on the table trash global settings page.
/admin/config/content/table_trash/global_settings

With that you should be in business! All you have to do is visit
/admin/config/content/table_trash to assign the decorations of your choice to
the tables that need them. That's it. Enjoy!

The current required extensions for full functionality are:
Styling
- Datatables
DataTables
- Datatables
Buttons
-Buttons
-Column Visibility
-HTML5 Export
-Print View
-JSZip
-pdfmake
-Print View
ColReorder
-ColReorder
FixedColumns
-FixedColumns
FixedHeader
-FixedHeader
Responsive
-Responsive
Packaging Options
-Minify
Concatenate
-Single file

NOTES AND CAVEATS

o To get the full benefits of fast sorting and paging, you're advised to switch
  OFF server-side sorting and paging where possible, e.g. in the Views UI.
  This is because the client-side sorting and paging functions provided by
  DataTables JS operate on the data downloaded with the latest browser request,
  which in case of server-side sorting is not the entire db query result set.

o The DataTables JS library does not cope well with tables that don't fully
  comply with modern HTML and omit the <thead> section, see
  http://datatables.net/forums/discussion/18273/datatables-requires-tables-to-have-a-thead
  Luckily nearly all tables on Drupal comply and work beautifully. These don't:
  admin/reports/status and admin/reports/updates.
  DataTables also does not like colspans anywhere in the table. If a table
  has a <td> with a colspan attribute, you get an error in your browser console.
  See http://datatables.net/forums/discussion/18274/datatables-does-not-cope-with-cols
  Problem pages: admin/people/permissions and admin/modules

o For the "Fixed left columns" feature and the export buttons to work on
  MULTIPLE tables on the same page, you have to create and configure a separate
  table decoration for EACH table, using a well-targeted CSS selector, at
  /admin/config/content/table_trash.

FORUMS

https://datatables.net/forums/discussions
