<?php

/**
 * @file
 * table_trash.admin.inc
 *
 * Configuration options for Table Trash module.
 */

/**
 * Menu callback for global settings configuration.
 */
function table_trash_admin_global_settings_form($form, &$form_state) {
  ctools_include('export');

  // Retain tree-hierarchy in values, prevent the form from being flattened.
  $form['#tree'] = TRUE;
  $form['global_settings']['#attached']['css'][] = drupal_get_path('module', 'table_trash') . '/css/table_trash.admin.css';

  $global_settings = variable_get('table_trash_global_settings', array());

  $form['global_settings']['load_from'] = array(
    '#type' => 'select',
    '#title' => t('Load library from:'),
    '#options' => array(
      'module' => t('Module'),
      'cdn' => t('Datatables CDN'),
      'alternate' => t('Alternate CDN'),
    ),
    '#default_value' => isset($global_settings['load_from']) ? $global_settings['load_from'] : 'module',
  );

  $form['global_settings']['alternate_cdn_js'] = array(
    '#type' => 'textfield',
    '#title' => t('Alternate js CDN url.'),
    '#maxlength' => 300,
    '#default_value' => isset($global_settings['alternate_cdn_js']) ? $global_settings['alternate_cdn_js'] : '',
    '#description' => t('An alternate cdn url. Note that these plugins are required for full functionality. JSZip, pdfmake, DataTables, AutoFill, Buttons, Column visibility, HTML5 export, Print view, ColReorder, FixedColumns, FixedHeader, KeyTable, Responsive, RowReorder, Scroller, Select.'),
    '#states' => array(
      'visible' => array(
        ':input[name="global_settings[load_from]"]' => array('value' => 'alternate'),
      ),
      'required' => array(
        ':input[name="global_settings[load_from]"]' => array('value' => 'alternate'),
      ),
    ),
  );

  $form['global_settings']['alternate_cdn_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Alternate css CDN url.'),
    '#maxlength' => 300,
    '#default_value' => isset($global_settings['alternate_cdn_css']) ? $global_settings['alternate_cdn_css'] : '',
    '#description' => t('An alternate cdn url. Note that these plugins are required for full functionality. JSZip, pdfmake, DataTables, AutoFill, Buttons, Column visibility, HTML5 export, Print view, ColReorder, FixedColumns, FixedHeader, KeyTable, Responsive, RowReorder, Scroller, Select.'),
    '#states' => array(
      'visible' => array(
        ':input[name="global_settings[load_from]"]' => array('value' => 'alternate'),
      ),
      'required' => array(
        ':input[name="global_settings[load_from]"]' => array('value' => 'alternate'),
      ),
    ),
  );

  $form['global_settings']['responsive'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="global-settings-responsive">',
    '#suffix' => '</div>',
  );

  $form['global_settings']['responsive']['breakpoint_phone'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 4,
    '#field_suffix' => t('px'),
    '#title' => t('Responsive width breakpoint for phone sized windows'),
    '#default_value' => isset($global_settings['responsive']['breakpoint_phone']) ? $global_settings['responsive']['breakpoint_phone'] : '',
    '#description' => t('The default width for phones is %px.', array('%px' => TABLE_TRASH_DEFAULT_BREAKPOINT_PHONE)),
  );

  $form['global_settings']['responsive']['breakpoint_tablet'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 4,
    '#field_suffix' => t('px'),
    '#title' => t('Responsive width breakpoint for tablet sized windows'),
    '#default_value' => isset($global_settings['responsive']['breakpoint_tablet']) ? $global_settings['responsive']['breakpoint_tablet'] : '',
    '#description' => t('The default width for tablets is %px.', array('%px' => TABLE_TRASH_DEFAULT_BREAKPOINT_TABLET)),
  );

  $form['global_settings']['use_datatables_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add native DataTables styling'),
    '#default_value' => isset($global_settings['use_datatables_css']) ? $global_settings['use_datatables_css'] : TRUE,
  );

  $form['global_settings']['use_table_trash_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add Table Trash styling'),
    '#default_value' => isset($global_settings['use_table_trash_css']) ? $global_settings['use_table_trash_css'] : TRUE,
  );

  $form['actions']['#type'] = 'actions';

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  $form['#theme'] = 'system_settings_form';

  return $form;
}

/**
 * Form submit handler for global settings form.
 */
function table_trash_admin_global_settings_form_submit($form, &$form_state) {
  // Clear out the form from stuff, like buttons, we do not wish to save.
  // @todo recursively sanitize (check_plain) all leaf values?).
  form_state_values_clean($form_state);

  $form_state['values']['decorations']['updated'] = TRUE;

  variable_set('table_trash_global_settings', $form_state['values']['global_settings']);

  // A change in the library files to be included requires clearing of the
  // Libraries cache. A call to libraries_flush_caches() is not sufficient here.
  cache_clear_all('*', 'cache_libraries', TRUE);

  drupal_set_message(t('Table decorations and global configuration have been saved.'));
}

/**
 * Menu callback for the update path.
 */
function table_trash_admin_update_config_form($form, &$form_state) {
  ctools_include('export');

  // Retain tree-hierarchy in values, prevent the form from being flattened.
  $form['#tree'] = TRUE;

  // Check if our table exists and that our variable still exists.
  if (db_table_exists('table_trash') && $decorations = variable_get('table_trash_decorations')) {

    // Form fields to set a Name and Machine Name for each existing decoration.
    for ($i = 1; $i <= count($decorations); $i++) {
      $form['decorations'][$i] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#title' => t('Table decoration #@no', array('@no' => $i)),
      );

      $form['decorations'][$i]['name'] = array(
        '#type' => 'textfield',
        '#maxlength' => 64,
        '#title' => t('Decoration Name'),
      );

      $form['decorations'][$i]['machine_name'] = array(
        '#type' => 'machine_name',
        '#maxlength' => 64,
        '#title' => t('Machine Name'),
        '#machine_name' => array(
          'exists' => 'table_trash_decoration_load',
          'source' => array('decorations', $i, 'name'),
          'label' => t('Machine Name'),
          'replace_pattern' => '[^a-z0-9-]+',
          'replace' => '_',
        ),
      );
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update Stored Decorations'),
    );
    $form['#validate'][] = 'table_trash_validate_machine_names';

    $form['delete']['message'] = array(
      '#markup' => "This will permanently delete the table_trash_decorations variable. Only press this after using the 'Update Stored Decorations' button.<br>",
    );

    $form['delete']['delete_variable'] = array(
      '#type' => 'submit',
      '#value' => 'Delete table_trash_decorations Variable',
      '#weight' => 1,
      '#submit' => array('_table_trash_delete_variable_submit'),
      '#limit_validation_errors' => array(),
    );

  }
  else {
    $form['message'] = array(
      '#markup' => t('No updates needed.'),
    );
  }

  return $form;
}

/**
 * Submit function to delete the old variable.
 */
function _table_trash_delete_variable_submit($form, &$form_state) {
  variable_del('table_trash_decorations');
}

/**
 * Validation function for table_trash_admin_update_config_form to make sure
 * all machine names are unique.
 */
function table_trash_validate_machine_names($form, &$form_state) {
  $machine_names = array();

  foreach ($form_state['values']['decorations'] as $key => $decoration) {
    $machine_names[$key] = $decoration['machine_name'];
  }

  $dups = array_count_values($machine_names);

  foreach ($machine_names as $key => $name) {
    if ($dups[$name] > 1) {
      form_set_error("decorations][$key][machine_name", "Machine names must be unique.");
    }
  }
}

/**
 * Load a single decoration.
 */
function table_trash_decoration_load($oid) {
  ctools_include('export');
  $result = ctools_export_load_object('table_trash', 'names', array($oid));
  if (isset($result[$oid])) {
    return $result[$oid];
  }
}

/**
 * Submit function for table_trash_admin_update_config_form.
 *
 * Saves all existing variable decorations as ctools exportable objects.
 */
function table_trash_admin_update_config_form_submit($form, &$form_state) {
  $decorations = variable_get('table_trash_decorations');

  // Update decorations to the new structure.
  $decorations = table_trash_update_variables($decorations);

  // Save decorations as objects.
  foreach ($form_state['values']['decorations'] as $key => $dec) {
    $decoration = ctools_export_crud_new('table_trash', TRUE);
    $decoration->api_version = 1;
    $decoration->machine_name = $dec['machine_name'];
    $decoration->name = $dec['name'];
    $decoration->data = $decorations[$key];

    // Process settings into a json object.
    table_trash_process_decoration_config($decoration);

    ctools_export_crud_save('table_trash', $decoration);
  }
}

/**
 * Takes the old variable settings array and converts it to the new structure.
 */
function table_trash_update_variables($decorations) {
  $global_settings = variable_get('table_trash_global_settings');

  $new_global_settings = array();

  $new_global_settings['responsive'] = [
    "breakpoint_phone" => $global_settings['responsive']['responsive-breakpoint-phone'],
    "breakpoint_tablet" => $global_settings['responsive']['responsive-breakpoint-tablet'],
  ];
  $new_global_settings['use_datatables_css'] = $global_settings['use-datatables-css'];
  $new_global_settings['use_table_trash_css'] = $global_settings['use-module-css'];

  variable_set('table_trash_global_settings', $new_global_settings);

  $new_decorations = array();
  foreach ($decorations as $key => $decoration) {
    if ($decoration['decoration-params']['export-buttons']) {
      $decoration['decoration-params']['export-buttons'] = drupal_map_assoc(array(
        'copy',
        'csv',
        'excel',
        'pdf',
        'print',
      ));
    }

    $new_decorations[$key]['decoration_config']['searching'] = $decoration['decoration-params']['search-box'];
    $new_decorations[$key]['decoration_config']['col_reorder'] = $decoration['decoration-params']['column-reorder'];
    $new_decorations[$key]['decoration_config']['buttons_export'] = $decoration['decoration-params']['export-buttons'];
    $new_decorations[$key]['decoration_config']['retrieve'] = $decoration['decoration-params']['retrieve-data'];
    $new_decorations[$key]['decoration_config']['paging_type'] = $decoration['decoration-params']['pager-style'];
    $new_decorations[$key]['decoration_config']['page_length'] = $decoration['decoration-params']['page-height'];
    $new_decorations[$key]['decoration_config']['not_orderable'] = $decoration['decoration-params']['dont-sort-columns'];
    $new_decorations[$key]['decoration_config']['scrollX'] = $decoration['decoration-params']['x-scroll'] ? 1 : 0;
    $new_decorations[$key]['decoration_config']['fixed_columns_left'] = $decoration['decoration-params']['fixed-left-columns'];
    $new_decorations[$key]['decoration_config']['fixed_header'] = $decoration['decoration-params']['fixed-header'];
    $new_decorations[$key]['decoration_config']['responsive_config']['responsive_control_column'] = $decoration['decoration-params']['responsive']['responsive-expand-col'];
    $new_decorations[$key]['decoration_config']['responsive_config']['responsive_phone_columns'] = $decoration['decoration-params']['responsive']['responsive-collapse-cols-phone'];
    $new_decorations[$key]['decoration_config']['responsive_config']['responsive_tablet_columns'] = $decoration['decoration-params']['responsive']['responsive-collapse-cols-tablet'];
    $new_decorations[$key]['selectors']['included_pages'] = $decoration['pages-and-selector']['include-pages'];
    $new_decorations[$key]['selectors']['excluded_pages'] = $decoration['pages-and-selector']['exclude-pages'];
    $new_decorations[$key]['selectors']['included_css_tables'] = $decoration['pages-and-selector']['table-selector'];
  }

  return $new_decorations;
}
