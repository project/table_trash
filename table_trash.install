<?php

/**
 * @file
 * Install and uninstall functions for the table_trash module.
 */

/**
 * Implements hook_enable().
 */
function table_trash_enable() {
  drupal_set_message(st("Thank you for installing Table Trash. If you have installed the DataTables JS library, then all you have to do now is create your <a href='@url'>table decoration</a> and you're done.", array(
      '@url' => url('admin/config/content/table_trash'))
  ));

  if (!variable_get('table_trash_global_settings', FALSE)) {
    variable_set('table_trash_global_settings', array(
      'load_from' => 'module',
      'use_datatables_css' => 1,
      'use_table_trash_css' => 1
    ));
  }
}

/**
 * Implements hook_uninstall().
 */
function table_trash_uninstall() {
  variable_del('table_trash_decorations');
  variable_del('table_trash_global_settings');
}

/**
 * Implements hook_schema().
 */
function table_trash_schema() {
  $schema['table_trash'] = array(
    'description' => 'Stores table trash configuration.',
    'export' => array(
      'key' => 'machine_name',
      'key name' => 'Machine Name',
      'identifier' => 'table_trash_decoration',
      'primary key' => 'machine_name',
      'default hook' => 'default_table_trash_decoration',
      'can disable' => FALSE,
      'admin_title' => 'name',
      'admin_description' => 'description',
      'api' => array(
        'owner' => 'table_trash',
        'api' => 'default_table_trash_decorations',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'did' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The decoration ID of the field, defined by the database.',
        'no export' => TRUE,
      ),
      'machine_name' => array(
        'description' => 'Exportable object machine name.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 64,
        'default' => '',
        'not null' => TRUE,
        'description' => 'The unique name of the decoration. This is the primary field decorations are loaded from, and is used so that decorations may be internal and not necessarily in the database. May only be alphanumeric characters plus underscores.',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => '255',
        'default' => '',
        'description' => 'A description of the decoration for the admin interface.',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The weight of this decoration in listings and the user interface.',
      ),
      'data' => array(
        'description' => 'Serialized data of exportable object.',
        'type' => 'blob',
        'not null' => TRUE,
        'size' => 'big',
        'serialize' => TRUE,
        'object default' => array(),
      ),
    ),
    'primary key' => array('did'),
    'unique keys' => array(
      'machine_name' => array('machine_name'),
    ),
  );

  return $schema;
}

/**
 * Create the table_trash table.
 */
function table_trash_update_7001() {
  $schema['table_trash'] = array(
    'description' => 'Stores table trash configuration.',
    'export' => array(
      'key' => 'machine_name',
      'key name' => 'Machine Name',
      'identifier' => 'table_trash_decoration',
      'primary key' => 'machine_name',
      'default hook' => 'default_table_trash_decoration',
      'can disable' => FALSE,
      'admin_title' => 'name',
      'admin_description' => 'description',
      'api' => array(
        'owner' => 'table_trash',
        'api' => 'default_table_trash_decorations',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'did' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The decoration ID of the field, defined by the database.',
        'no export' => TRUE,
      ),
      'machine_name' => array(
        'description' => 'Exportable object machine name.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 64,
        'default' => '',
        'not null' => TRUE,
        'description' => 'The unique name of the decoration. This is the primary field decorations are loaded from, and is used so that decorations may be internal and not necessarily in the database. May only be alphanumeric characters plus underscores.',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => '255',
        'default' => '',
        'description' => 'A description of the decoration for the admin interface.',
      ),
      'weight' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The weight of this decoration in listings and the user interface.',
      ),
      'data' => array(
        'description' => 'Serialized data of exportable object.',
        'type' => 'blob',
        'not null' => TRUE,
        'size' => 'big',
        'serialize' => TRUE,
        'object default' => array(),
      ),
    ),
    'primary key' => array('did'),
    'unique keys' => array(
      'machine_name' => array('machine_name'),
    ),
  );

  db_create_table('table_trash', $schema['table_trash']);
}
